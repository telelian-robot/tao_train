# 0.Prepare
ngc 는 설치 되어있어야 한다. 

https://github.com/zexihan/labelImg-kitti 다운로드 

이미지를 준비하고 script/crop_and_resize.py 실행하여 train image 생성 ( 소스 보고 사이즈랑 필요한 부분 고치면 된다. )

위 labelImg kitti 를 이용해 raw_label 을 만들자 

script/parse.py 를 이용하여 raw_label -> train_label 로 parsing 하자 ! 


```bash 
# 개인 workspace export 
git clone https://gitlab.com/telelian-robot/tao_train.git
cd tao_train
export TAO_WS=$PWD

ngc config set
#API key 입력후 전부 enter 로 넘어 가면 된다.
```

# 1.Downolad pretrained model
```bash
cd models/0_pretrained/
ngc registry model download-version nvidia/tao/pretrained_detectnet_v2:resnet18
```

# 2.Dataset convert ( kitti or coco -> tfrecord)
```bash
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 dataset_convert \
-d /workspace/spec_file/kitti_config.txt \
-o /workspace/tf_records/
```

# 3.Train 
```bash
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 train \
-e /workspace/spec_file/train.txt \
-r /workspace/models/1_trained \
-k tlt_encode
```

# 4.Evaluation
```bash
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 evaluate \
-e /workspace/spec_file/train.txt \
-m /workspace/models/1_trained/weights/model.tlt \
-k tlt_encode
```

# 5.Pruning ( 가지치기 / 모델 경량화 )
```bash
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 prune \
-m /workspace/models/1_trained/weights/model.tlt \
-o /workspace/models/2_pruned/model.tlt \
-k tlt_encode
```

# 6.Retrain prune model
```bash
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 train \
-e /workspace/spec_file/retrain.txt \
-r /workspace/models/3_retrained \
-k tlt_encode
```

# 7.Inference ( 만든 모델 검증 ) -- 생략 해도 됨
```bash
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 inference  \
-i /workspace/dataset/test_image/ \
-e /workspace/spec_file/inference.txt \
-o /workspace/inference \
-k tlt_encode
```

# 8.export
```bash
# generate calibration file
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 calibration_tensorfile \
-e /workspace/spec_file/retrain.txt \
-o /workspace/models/4_exported/calibration.tensor \
-m 20

# export
docker run -it --rm --gpus all -v $TAO_WS:/workspace/ \
nvcr.io/nvidia/tao/tao-toolkit:4.0.0-tf1.15.5 \
detectnet_v2 export \
-e /workspace/spec_file/retrain.txt \
-m /workspace/models/3_retrained/weights/model.tlt \
-o /workspace/models/4_exported/model.etlt \
--cal_data_file /workspace/models/4_exported/calibration.tensor \
--gen_ds_config \
-k tlt_encode
```
