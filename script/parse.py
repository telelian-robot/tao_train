import os

root_path='dataset/raw_label/'
parse_path='dataset/train_labels/'

label_list = os.listdir(root_path)

for label_name in label_list:
    label = open(root_path+label_name)
    label_buf = label.read()
    label_buf = label_buf.split(' ')

    x = float(label_buf[1])
    y = float(label_buf[2])
    w = float(label_buf[3])
    h = float(label_buf[4])

    left = round(x - w/2, 3)
    top = round(y - h/2, 3)
    right = round(x + w/2, 3)
    bottom = round(y + h/2, 3)

    parse_label = open(parse_path+label_name, 'w')
    parse_label.write('fan 0.00 0 0.00 '+str(left)+' '+str(top)+' '+str(right)+' '+str(bottom)+' 0.00 0.00 0.00 0.00 0.00 0.00 0.00')

    
