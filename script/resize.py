from PIL import Image
import os


root_path='dataset/raw_image/'
resize_path='dataset/train_image/'

image_list = os.listdir(root_path)


def image_resize(image, width, height, save_path):
    image = image.resize((width, height), Image.ANTIALIAS)
    image.save(save_path)


i = 1
for image_name in image_list:
    image = Image.open(root_path+image_name)
    width, height = image.size
    print(width, height)
    image_resize(image, 480, 320, resize_path+str(i)+'.jpg')
    i += 1


resize_image_list = os.listdir(resize_path)

for image_name in resize_image_list:
    image = Image.open(resize_path+image_name)
    width, height = image.size
    print(width, height)