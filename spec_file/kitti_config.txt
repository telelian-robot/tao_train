kitti_config {
  root_directory_path: "/workspace/dataset"
  image_dir_name: "train_image"
  label_dir_name: "train_label"
  image_extension: ".jpg"
  partition_mode: "random"
  num_partitions:2
  val_split: 20
  num_shards: 10
}
image_directory_path: "/workspace/dataset"